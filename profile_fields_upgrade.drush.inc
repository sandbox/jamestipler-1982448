<?php

/**
 * @file
 * A module to migrate fields from the deprecated core user profiles to 
 * fields on the user object where they belong. 4 years. FOUR YEARS people
 * since this issue was raised. I am not pleased that I have to be doing 
 * this myself at this point. This is the DRUSH bit.
 */

function profile_fields_upgrade_drush_command() {
  $items = array();

  $items['profile-fields-upgrade'] = array(
    'description' => "Upgrades core-profile fields to fields.api fields and attaches them to the user object. Optionally migrates data from those fields.",
    'arguments' => array(
      'field' => "The field to upgrade - or \"all\" to migrate all fields.",
      ),
    'options' => array(
      'list' => "Lists all discovered core-profile fields.",
      'no-migrate' => "Do not migrate any data, just create the fields.",
      ),
    'examples' => array(
      'drush prup --list' => "lists all core-profile fields.",
      'drush prup profile_phone' => "upgrades the \"profile_phone\" field and migrates it's data.",
      'drush prup all --no-migrate' => "upgrades all core-profile fields, but does not migrate any data.",
      ),
    'aliases' => array('prup'),
    );

  return $items;
  }

function drush_profile_fields_upgrade($field = NULL) {

  $q = db_select('profile_field', 'p')
    ->fields('p')
    ->orderBy('category');

  $result = $q->execute();

  $upgraded = array();
  $not_upgraded = array();

  while ( $record = $result->fetchAssoc() ) {

    $shortform = substr("field_" . $record['name'], 0, 31);

    if (field_info_field("field_" . $record['name'])) {
      array_push($upgraded, $record['name']);
      }
    elseif (field_info_field($shortform)) {
      array_push($upgraded, $record['name']);
      }
    elseif (!field_info_field("field_" . $record['name'])) {
      array_push($not_upgraded, $record['name']);
      }
    }

  if (drush_get_option('list')) {

    echo "\n";
    if (count($upgraded) == 0) {
      echo "No fields upgraded so far. \n\n";
      }
    else {
      echo "The following fields have been upgraded already: \n";
      foreach ($upgraded as $field) {
        echo "\t" . $field;
        if ( strlen($field) > 26 ) {
          echo " <- Long name. May have been truncated. ";
          }
        echo "\n";
        }
      echo "\n";
      }
    if (count($not_upgraded) == 0) {
      echo "No fields available to upgrade. \n\n";
      }
    else {
      echo "The following fields can be upgraded: \n";
      foreach ($not_upgraded as $field) {
        echo "\t" . $field;
        if ( strlen($field) > 26 ) {
          echo " <- Long name. Will been truncated. ";
          }
        echo "\n";
        }
      echo "\n";
      }
    return TRUE;
    }

  if ($field == NULL) {
    echo "Usage: prup [field] --[options]. \n";
    return FALSE;
    }
  else {

    if ($field == "all") {
      echo "Right you are, upgrading every core-profile field I can find... this might take a while. \n\n";
    
      foreach ($not_upgraded as $upgrade_this) {
        drush_profile_fields_upgrade_setup($upgrade_this);
        }

      return TRUE;
      }
    elseif (!in_array($field, $not_upgraded)) {
      if (!in_array($field, $upgraded)) {
        echo "The specified field doesn't appear to exist... Did you typo something? \n\n";
        return FALSE;
        }
      else {
        echo "That field appears to have been upgraded already. Nothing to do here. \n\n";
        return FALSE;
        }
      }
    else {
      drush_profile_fields_upgrade_setup($field);
      }
    }
  }

function drush_profile_fields_upgrade_setup($field) {

  echo "Upgrading $field please wait...\n";
  if (!drush_profile_fields_upgrade_do($field)) {
    echo "Eeeek. Something seems to have gone rather wrong there... you'd better check the database becuase I don't know what happened. \n";
    }
  else {
    echo "\t...Done! \n";
  
    if (drush_get_option('no-migrate')) {
      echo "You asked for no migration, so we're done here.\n";
      return TRUE;
      }
    else {
      echo "Migrating entries to $field... \n";
      $v = drush_profile_fields_upgrade_migrate_do($field);
        
      if (!$v) {
        echo "The field migration seems to have failed. Check the database. \n";
        }
      else {
        echo "\t $v entries migrated to the new $field field. \n";
        }
      }
    }
  }



function drush_profile_fields_upgrade_do($field_up) {

  $q = db_select('profile_field', 'p')
    ->fields('p')
    ->orderBy('category');

  $result = $q->execute();

  while ( $record = $result->fetchAssoc() ) {

    if ( $record['name'] == $field_up ) { 
      $r = "<p>";

      if (!field_info_field('field_' . $record['name'])) {

        if ($record['required'] == 1) {
          $required = TRUE;
          }
        else {
          $required = FALSE;
          }
    
      switch ( $record['type'] ) {
        case "textfield":
          $field = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'type' => "text",
            'label' => $record['title'],
            );

          $fieldinstance = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'bundle' => 'user',
            'label' => $record['title'],
            'settings' => array(
              'user_register_form' => intval($record['register']),
              ),
            'required' => $required,
            'description' => $record['explanation'],
            );

          break;
        case "url":
          $field = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'type' => "text",
            'label' => $record['title'],
            );

          $fieldinstance = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'bundle' => 'user',
            'label' => $record['title'],
            'settings' => array(
              'user_register_form' => intval($record['register']),
              ),
            'required' => $required,
            'description' => $record['explanation'],
            );
          break;
        case "text_area":
          $field = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'type' => "text_long",
            'label' => $record['title'],
            );

          $fieldinstance = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'bundle' => 'user',
            'label' => $record['title'],
            'settings' => array(
              'user_register_form' => intval($record['register']),
              ),
            'required' => $required,
            'description' => $record['explanation'],
            );
          break;
        case "list":
          $field = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'type' => "text_long",
            'label' => $record['title'],
            );

          $fieldinstance = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'bundle' => 'user',
            'label' => $record['title'],
            'settings' => array(
              'user_register_form' => intval($record['register']),
              ),
            'required' => $required,
            'description' => $record['explanation'],
            );
          break;    
        case "selection":
          $allowed_values = explode("\n", $record['options']);
          $values = array();
          foreach ($allowed_values as $allowed_value) {
            $value = trim($allowed_value);
            $values[$value] = $value;
            }
          $allowed_values = $values;
          $field = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'type' => "list_text",
            'label' => $record['title'],
            'settings' => array(
              'allowed_values' => $allowed_values,
              ),
            );

          $fieldinstance = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'bundle' => 'user',
            'label' => $record['title'],
            'settings' => array(
              'user_register_form' => intval($record['register']),
              ),
            'required' => $required,
            'description' => $record['explanation'],
            );
          break;
        case "checkbox":
          $field = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'type' => "list_boolean",
            'label' => $record['title'],
            'settings' => array(
              'allowed_values' => array(
                '0' => 0,
                '1' => 1,
                ),
              ),
            );

          $fieldinstance = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'bundle' => 'user',
            'label' => $record['title'],
            'settings' => array(
              'user_register_form' => intval($record['register']),
              ),
            'required' => $required,
            'description' => $record['explanation'],
            'widget' => array(
              'type' => "options_onoff",
              'settings' => array(
                'display_label' => "1",
                ),
              ),
            );
          break;
        case "date":
          $field = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'type' => "date",
            'label' => $record['title'],
            );

          $fieldinstance = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'bundle' => 'user',
            'label' => $record['title'],
            'settings' => array(
              'user_register_form' => intval($record['register']),
              ),
            'required' => $required,
            'description' => $record['explanation'],
            );
          break;
        }

        if ( strlen($field['field_name']) > 31) {

          $field['field_name'] = substr($field['field_name'], 0, 31);
          $fieldinstance['field_name'] = $field['field_name'];
          echo "Truncating field name to " . $field['field_name'] . " \n";
          }

         $createfield = field_create_field($field);
        if (!$createfield ) {
          echo "Failed to create field field_$field. I'm bugging out. \n";
          return FALSE;
          }

        $createinstance = field_create_instance($fieldinstance);
        if (!$createinstance ) {
          echo "Failed to create field instance for field_$field. I'm bugging out. \n";
          return FALSE;
          }
        }
      }
    }
  return TRUE;
  }


function drush_profile_fields_upgrade_migrate_do($field) {

  $i = 0;

  $field_fix = "field_" . $field;

  
  if ( strlen("field_" . $field) > 31) {
    echo "Stupidly long name detected. Truncating... \n";
    $field_fix = substr($field_fix, 0, 31);
    }


  $migrate_field = db_select('profile_field', 'p')
    ->fields('p')
    ->condition('name', $field , '=');
  $migrate_field = $migrate_field->execute();

  $pfield = $migrate_field->fetchAssoc();

  $migrate_data = db_select('profile_value', 'p')
    ->fields('p')
    ->distinct()
    ->condition('fid', $pfield['fid'] , '=');
  
  $migrate_rows = $migrate_data->execute();

  while ( $record = $migrate_rows ->fetchAssoc() ) {

    $write = array(
      'entity_type' => "user",
      'bundle' => "user",
      'delta' => 0,
      'language' => "und",
      'entity_id' => $record['uid'],
      'revision_id' => $record['uid'],
      $field_fix . '_value' => $record['value'],
      );

    $data = db_insert('field_data_' . $field_fix)->fields($write)->execute();
    $revision = db_insert('field_revision_' . $field_fix)->fields($write)->execute();
    $i++;        
    }

  return $i;
  }