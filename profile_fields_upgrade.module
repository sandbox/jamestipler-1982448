<?php

/**
 * @file
 * A module to migrate fields from the deprecated core user profiles to 
 * fields on the user object where they belong. 4 years. FOUR YEARS people
 * since this issue was raised. I am not pleased that I have to be doing 
 * this myself at this point. 
 */





/**
 * Implements hook_help.
 *
 * Displays help and module information.
 *
 * @param path 
 *   Which path of the site we're using to display help
 * @param arg 
 *   Array that holds the current path as returned from arg() function
 */

function profile_fields_upgrade_help($path, $arg) {

  switch ($path) {
    case "admin/help#profile_fields_upgrade":
      $return = '<p>' . t("Upgrades fields from the core-profile module to regular fields on the user object. It is utterly ridiculous that I'm having to write a module to do this four years after the problem was raised.") . '</p>';
      $return .= "<p>All profile_field data types are supported, however not all options within those types are, so check the output after this has run.";
      $return .= " Required fields and fields shown on the registration form will remain so, and the following conversions will occur:</p><ul>";
      $return .= "<li> checkbox -&gt; list_boolean </li>"; 
      $return .= "<li> list -&gt; text_long </li>"; 
      $return .= "<li> selection -&gt; list_text </li>"; 
      $return .= "<li> url -&gt; text </li>"; 
      $return .= "</ul><p>the new fields will be named field_[oldfieldname] in accordance with the fields.api naming convention. This has the potential to cause excessively long names, and any that come out over 32 characters will be truncated upon conversion.</p>";
      $return .= "<p>It is <strong>strongly</strong> recommended that you use the included drush commands rather than the GUI. The conversion of the fields happens almost instantly, but migrating data can take in the order of 1 minute per 10,000 enteries. If you have a lot of users, or migrate a lot of fields at once, it's easy to breach the PHP max execution time and cause the process to fail, so again, <strong>USE DRUSH</strong>.</p>";
      $return .= "<p>This module is not well tested, nor is it well supported. I'll fix any bugs reported as and when I have time to do so, but I thought it was better to provide something than nothing.</p>";


    return $return;
    break;
    }
  }

/**
 * Implements hook_menu().
 */
function profile_fields_upgrade_menu() {
  $items = array();

  $items['admin/config/people/profilefields'] = array(
    'title' => 'Profile Fields Upgrade',
    'description' => 'Configuration for Profile Fields Upgrade module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('profile_fields_upgrade_form'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_NORMAL_ITEM,
    );

  return $items;
  }

/**
 * Page callback: Profile Fields Upgrade settings
 *
 * @see profile_fields_upgrade_menu()
 */

function profile_fields_upgrade_form($form_state) {

  $header = array(
    'field_name' => t('Field name'),
    'field_type' => t('Field Type'),
    'field_bundle' => t('Field bundle'),
    );
  $options = array();
  

  $r = "<p>" . t("Upgrades fields from the core-profile module to regular fields on the user object. It is utterly ridiculous that I'm having to write a module to do this four years after the problem was raised.") . "</p>";

  $return = "<p>All fields that were detected from the profile module that can be upgraded are listed below. If a field has already been upgraded, or there is already a field with the same name on the user object, then it will not show up as available for upgrade.</p>";
  $return .= "<p>You don't get a lot of choices, just chose the ones you want to migrate then press start. The selected fields will be upgraded to fields.api fields, and their data migrated.</p>";
  $return .= "<p>This doesn't currently use the batch processing API so if there are a lot of datapoints to migrate, there's a good chance you'll hit the PHP maximum execution time. It's strongly recommended that you use the included drush command \"prup\" instead of this GUI when migrating more than single fields, unless you know that there is only a small quantity of data.</p>";

  $q = db_select('profile_field', 'p')
    ->fields('p')
    ->orderBy('category');

  $result = $q->execute();

  $upgraded = "If there are any fields that have been upgraded already they'll be listed below. It is possible that fields not upgraded from the profile module, but that happen to have the same name and already exist, will show up here. If that's the case then you'll not be able to upgrade the associated profile fields until you remove / rename the existing ones.  <br/>";
  $upgraded .= "<table><tr><th>Fields already upgraded</th><th>Fields.api field name</th></tr>";

  while ( $record = $result->fetchAssoc() ) {

    $shortform = substr("field_" . $record['name'], 0, 31);

    if (field_info_field("field_" . $record['name'])) {
      $upgraded .= "<tr><td>" . $record['title'] . "</td><td>field_" . $record['name'] . "</td></tr>";
      }
   elseif (field_info_field($shortform)) {
      $upgraded .= "<tr><td>" . $record['title'] . " <td>" . $shortform . "</td></tr>";
      }

    elseif (!field_info_field("field_" . $record['name'])) {

      $q2 = db_select('profile_value', 'v')
        ->fields('v')
        ->condition('v.fid', $record['fid'], '=');

      $r2 = $q2->execute();
      $enteries = $r2->rowCount();



      $options[$record['fid']] = array(
        'field_name' => $record['title'],
        'field_type' => $record['type'],
        'field_bundle' => $record['category'],
        );
      }
    }

  $upgraded .= "</table>";
  

  $form['explanation'] = array(
    '#markup' => $r,
    );

  $form['discovered'] = array(
    '#markup' => $return,
    );

  $form['tableoptions'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#multiple' => TRUE,
    );

  $form['upgraded'] = array(
    '#markup' => $upgraded,
    );

  $form['start'] = array(
    '#type' => 'submit',
    '#title' => 'Start',
    '#value' => t('Start'),
    );

  return $form;
  }


/**
 * Export Start Form Validate
 */
function profile_fields_upgrade_form_validate($form, &$form_state) {
  

  $q = db_select('profile_field', 'p')
    ->fields('p')
    ->orderBy('category');

  $result = $q->execute();

  while ( $record = $result->fetchAssoc() ) {

    if ( in_array($record['fid'], $form_state['values']['tableoptions']) ) { 
      $r = "<p>";

      if (!field_info_field('field_' . $record['name'])) {

        if ($record['required'] == 1) {
          $required = TRUE;
          }
        else {
          $required = FALSE;
          }
    
      switch ($record['type']) {
        case "textfield":
          $field = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'type' => "text",
            'label' => $record['title'],
            );

          $fieldinstance = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'bundle' => 'user',
            'label' => $record['title'],
            'settings' => array(
              'user_register_form' => intval($record['register']),
              ),
            'required' => $required,
            'description' => $record['explanation'],
            );

          break;
        case "url":
          $field = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'type' => "text",
            'label' => $record['title'],
            );

          $fieldinstance = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'bundle' => 'user',
            'label' => $record['title'],
            'settings' => array(
              'user_register_form' => intval($record['register']),
              ),
            'required' => $required,
            'description' => $record['explanation'],
            );
          break;
        case "text_area":
          $field = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'type' => "text_long",
            'label' => $record['title'],
            );

          $fieldinstance = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'bundle' => 'user',
            'label' => $record['title'],
            'settings' => array(
              'user_register_form' => intval($record['register']),
              ),
            'required' => $required,
            'description' => $record['explanation'],
            );
          break;
        case "list":
          $field = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'type' => "text_long",
            'label' => $record['title'],
            );

          $fieldinstance = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'bundle' => 'user',
            'label' => $record['title'],
            'settings' => array(
              'user_register_form' => intval($record['register']),
              ),
            'required' => $required,
            'description' => $record['explanation'],
            );
          break;    
        case "selection":
          $allowed_values = explode("\n", $record['options']);
          $values = array();
          foreach ($allowed_values as $allowed_value) {
            $value = trim($allowed_value);
            $values[$value] = $value;
            }
          $allowed_values = $values;
          $field = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'type' => "list_text",
            'label' => $record['title'],
            'settings' => array(
              'allowed_values' => $allowed_values,
              ),
            );

          $fieldinstance = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'bundle' => 'user',
            'label' => $record['title'],
            'settings' => array(
              'user_register_form' => intval($record['register']),
              ),
            'required' => $required,
            'description' => $record['explanation'],
            );
          break;
        case "checkbox":
          $field = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'type' => "list_boolean",
            'label' => $record['title'],
            'settings' => array(
              'allowed_values' => array(
                '0' => 0,
                '1' => 1,
                ),
              ),
            );

          $fieldinstance = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'bundle' => 'user',
            'label' => $record['title'],
            'settings' => array(
              'user_register_form' => intval($record['register']),
              ),
            'required' => $required,
            'description' => $record['explanation'],
            'widget' => array(
              'type' => "options_onoff",
              'settings' => array(
                'display_label' => "1",
                ),
              ),
            );
          break;
        case "date":
          $field = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'type' => "date",
            'label' => $record['title'],
            );

          $fieldinstance = array(
            'field_name' => "field_" . $record['name'],
            'entity_type' => 'user',
            'bundle' => 'user',
            'label' => $record['title'],
            'settings' => array(
              'user_register_form' => intval($record['register']),
              ),
            'required' => $required,
            'description' => $record['explanation'],
            );
          break;
        }

        $createfield = field_create_field($field);

        if (!$createfield ) {
          form_set_error("Could not create field field_" . $record['name'] . "!");
          }
        else {
          $r .= "created field field_" . $record['name'] . " successfully. <br>";  
          }


          $createinstance = field_create_instance($fieldinstance);

        if (!$createinstance ) {
          form_set_error("Could not create instance of field field_" . $record['name'] . " on user object!");
          }
        else {
          $r .= "created instance of field field_" . $record['name'] . " on the user object. <br>";  
          }

        }
      $r .= "</p>";
      drupal_set_message(filter_xss($r));
      }

    }

  }

/**
 * Export Start Form Submit
 */
function profile_fields_upgrade_form_submit($form, &$form_state) {
  
  $i = array();
  foreach ($form_state['values']['tableoptions'] as $field) {
    if ($field != 0) {

      $q = db_select('profile_value', 'p')
        ->fields('p')
        ->distinct()
        ->condition('fid', $field , '=');
  
      $result = $q->execute();

      $q2 = db_select('profile_field', 'p')
        ->fields('p')
        ->condition('fid', $field , '=');
        $r2 = $q2->execute();

        $pfield = $r2->fetchAssoc();
      
      $i[$pfield['name']] = 0;

  
      while ( $record = $result->fetchAssoc() ) {

        $write = array(
          'entity_type' => "user",
          'bundle' => "user",
          'delta' => 0,
          'language' => "und",
          'entity_id' => $record['uid'],
          'revision_id' => $record['uid'],
          'field_' . $pfield['name'] . '_value' => $record['value'],
          );

        $data = db_insert('field_data_field_' . $pfield['name'])->fields($write)->execute();
        $revision = db_insert('field_revision_field_' . $pfield['name'])->fields($write)->execute();

        $i[$pfield['name']]++;        
        }
      }
    }


  $f = "<ul>";
  foreach ($i as $key => $value)
      $f .= "<li>Migrated " . $value . " values to field_" . $key . "</li>";
  $f .= "</ul>";



  drupal_set_message(filter_xss($f));

  }
